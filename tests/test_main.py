import io
import unittest
from unittest import mock

from scruf import compare, execute, main, parsers, test
from tests import factories, utils


def _build_test_observer():
    return utils.NullObserver()


def _build_basic_options():
    return {
        "shell": "/bin/sh",
        "cleanup": True,
        "indent": "    ",
        "env": None,
        "strict": False,
    }


class TestRun(unittest.TestCase):
    def test_options_are_passed_down(self):
        args = [
            "prog_name",
            "--shell",
            "/bin/sh",
            "--no-cleanup",
            "--indent",
            r"\t",
            "--env-file",
            "my_env.cfg",
            "--strict",
            "foo",
            "bar",
        ]
        env_config = """
        [Scruf Env]
        PATH=/bin:/usr/bin
        """
        env = {"PATH": "/bin:/usr/bin"}
        filenames = ["foo", "bar"]
        options = {
            "shell": "/bin/sh",
            "cleanup": False,
            "indent": "\t",
            "env": env,
            "strict": True,
        }
        with mock.patch("scruf.main.run_files") as run_files_mock:
            # python3.6 doesn't like 'mock.mock_open' here, so use io.StringIO
            with mock.patch("builtins.open", return_value=io.StringIO(env_config)):
                main.run(args)
        run_files_args = run_files_mock.call_args[0]
        self.assertEqual(run_files_args[0], filenames)
        self.assertEqual(run_files_args[1], options)

    def test_option_defaulting(self):
        filename = "test_file"
        args = ["prog_name", filename]
        expected_options = {
            "shell": "/bin/sh",
            "cleanup": True,
            "indent": "    ",
            "env": None,
            "strict": False,
        }
        with mock.patch("scruf.main.run_files") as run_files_mock:
            with mock.patch("sys.exit"):
                main.run(args)
        run_files_args = run_files_mock.call_args[0]
        self.assertEqual(run_files_args[0], [filename])
        self.assertEqual(run_files_args[1], expected_options)

    def test_with_unparseable_env_file(self):
        filename = "placeholder"
        config_filename = "my_env.cfg"
        args = ["prog_name", "--env-file", config_filename, filename]

        with mock.patch(
            "builtins.open", return_value=io.StringIO("Invalid configuration")
        ):
            with utils.Capturer() as captured:
                return_val = main.run(args)
        self.assertEqual(return_val, 1)
        self.assertRegex(
            captured.stderr,
            "Failed to process options: Unable to parse config file '"
            + config_filename
            + "'",
        )

    def test_with_env_file_missing_required_key(self):
        filename = "placeholder"
        config_filename = "missing_section.cfg"
        args = ["prog_name", "--env-file", config_filename, filename]

        with mock.patch("builtins.open", mock.mock_open(read_data="")):
            with utils.Capturer() as captured:
                return_val = main.run(args)
        self.assertEqual(return_val, 1)
        self.assertEqual(
            captured.stderr,
            "Failed to process options: Could not find expected section: "
            + "'Scruf Env' in config file: "
            + config_filename
            + "\n",
        )

    def test_indent_with_non_whitespace(self):
        filename = "placeholder"
        indent = "a"
        args = ["prog_name", "--indent", indent, filename]

        with utils.Capturer():  # Capture and suppress output
            with self.assertRaises(SystemExit) as cm:
                main.run(args)
        self.assertEqual(cm.exception.code, 2)

    def test_run_success(self):
        with mock.patch("scruf.main.run_files", return_value=True):
            exit_code = main.run(["prog_name", "filename"])
        self.assertEqual(exit_code, 0)

    def test_run_failure(self):
        with mock.patch("scruf.main.run_files", return_value=False):
            exit_code = main.run(["prog_name", "filename"])
        self.assertEqual(exit_code, 1)


class TestRunFiles(unittest.TestCase):
    def test_with_all_success(self):
        filenames = ["foo", "bar"]
        options = {}
        with mock.patch("scruf.main.run_file", return_value=True):
            self.assertTrue(main.run_files(filenames, options, _build_test_observer()))

    def test_with_a_failure(self):
        filenames = ["foo", "bar"]
        options = {}
        with mock.patch("scruf.main.run_file", side_effect=[False, True]):
            self.assertFalse(main.run_files(filenames, options, _build_test_observer()))


class TestRunFile(unittest.TestCase):
    def test_run(self):
        file_contents = "# foo bar\n"
        filename = "test.t"

        with mock.patch("scruf.main.run_tests", return_value=True):
            with mock.patch(
                "scruf.main.open", mock.mock_open(read_data=file_contents)
            ) as m:
                success = main.run_file(
                    filename, _build_basic_options(), _build_test_observer()
                )

        m.assert_called_once_with(filename, "r")
        self.assertTrue(success)

    def test_run_with_open_exception(self):
        error_string = "File not found"
        filename = "not_a_file"
        main.open = mock.Mock(side_effect=FileNotFoundError(error_string))

        success = main.run_file(
            filename, _build_basic_options(), _build_test_observer()
        )
        self.assertFalse(success)

    def test_run_with_parse_error(self):
        filename = "foo"

        with mock.patch("scruf.main.open", mock.mock_open(read_data="placeholder")):
            with mock.patch(
                "scruf.parsers.TestParser.parse",
                side_effect=parsers.ProgressionError(
                    1, "foo bar", "start", ["description", "command"]
                ),
            ):
                success = main.run_file(
                    filename, _build_basic_options(), _build_test_observer()
                )
        self.assertFalse(success)

    def test_run_with_execute_error(self):
        filename = "foo"
        test_dir = "/tmp"
        file_error = PermissionError("You don't have permission")

        with mock.patch("scruf.main.open", mock.mock_open(read_data="placeholder")):
            with mock.patch(
                "scruf.execute.Executor",
                side_effect=execute.FailedToCreateTestDirError(test_dir, file_error),
            ):
                success = main.run_file(
                    filename, _build_basic_options(), _build_test_observer()
                )
        self.assertFalse(success)

    def test_executor_options(self):
        options = {"shell": "/bin/bash", "cleanup": False, "env": {}, "strict": False}
        options["indent"] = " "  # not used, but a required key
        with mock.patch("scruf.main.open", mock.mock_open(read_data="placeholder")):
            with mock.patch("scruf.main.run_tests"):
                with mock.patch("scruf.parsers.TestParser.parse"):
                    with mock.patch("scruf.execute.Executor") as executor_mock:
                        main.run_file("test_file", options, _build_test_observer())

        executor_mock.assert_called_once_with(
            shell=options["shell"], cleanup=options["cleanup"], env=options["env"]
        )


class TestRunFileObserverNotifications(unittest.TestCase):
    def test_observations_with_file_error(self):
        observer = mock.Mock()
        error = PermissionError("You don't have permission")
        filename = "no_read_permission"

        with mock.patch("scruf.main.open", side_effect=error):
            main.run_file(filename, _build_basic_options(), observer)
        observer.notify_file_open_error.assert_called_once_with(filename, error)

    def test_observations_with_progression_error(self):
        observer = mock.Mock()
        error = parsers.ProgressionError(
            1, "Bad line content", "command", ["description", "continue"]
        )
        filename = "bad_test_contents_in_here"

        with mock.patch("scruf.main.open", mock.mock_open(read_data="placeholder")):
            with mock.patch("scruf.parsers.TestParser.parse", side_effect=error):
                main.run_file(filename, _build_basic_options(), observer)
        observer.notify_before_testing_file.assert_called_once_with(filename)
        observer.notify_test_progression_error.assert_called_once_with(filename, error)

    def test_observations_with_execute_setup_error(self):
        observer = mock.Mock()
        filename = "some_test_file"
        error = execute.FailedToCreateTestDirError(
            "/root", PermissionError("Can't touch this")
        )

        with mock.patch("scruf.main.open", mock.mock_open(read_data="placeholder")):
            with mock.patch("scruf.parsers.TestParser.parse", return_value=[]):
                with mock.patch("scruf.execute.Executor", side_effect=error):
                    main.run_file(filename, _build_basic_options(), observer)
        observer.notify_before_testing_file.assert_called_once_with(filename)
        observer.notify_execute_setup_error.assert_called_once_with(filename, error)


class TestRunTests(unittest.TestCase):
    def test_successful_test(self):
        observer = utils.NullObserver()
        t = test.Test(
            description="Basic successful test",
            setup_commands=["echo 'some_text' > test.txt'"],
            command="printf 'foo\n1bar\n'\n",
            output_specs=["foo\n", "[RE] [0-9]bar\n", "[0]\n"],
        )
        result = execute.Output(returncode=0, stdout="foo\n1bar\n")
        executor = utils.mock_executor(result)
        comparison_results = [factories.runner_result_factory(success=True)]

        with mock.patch("scruf.runner.compare_result", return_value=comparison_results):
            success = main.run_tests([t], executor, observer)
        self.assertTrue(success)

    def test_failing_test(self):
        observer = utils.NullObserver()
        t = test.Test(
            command="echo 'foo'\n",
            description="Basic comparison failure\n",
            output_specs=["bar\n"],
        )
        result = execute.Output(returncode=0, stdout="foo\n")
        executor = utils.mock_executor(result)
        comparison_results = [factories.runner_result_factory(success=False)]

        with mock.patch("scruf.runner.compare_result", return_value=comparison_results):
            success = main.run_tests([t], executor, observer)
        self.assertFalse(success)

    def test_successful_comparison_but_strict_failure(self):
        result_content = "foo\nbar\n"
        for result in (
            execute.Output(returncode=0, stdout=result_content),
            execute.Output(returncode=0, stderr=result_content),
        ):
            observer = utils.NullObserver()
            t = test.Test(
                command="printf 'foo\nbar\n'\n",
                description="Strict failure\n",
                output_specs=[parsers.OutputParser.parse("foo\n")],
            )
            result = execute.Output(returncode=0, stdout="foo\nbar\n")
            executor = utils.mock_executor(result)

            self.assertFalse(main.run_tests([t], executor, observer, strict=True))

    def test_failing_setup_command(self):
        observer = utils.NullObserver()
        t = test.Test("echo 'abc123'\n", setup_commands=["cat /not/a/real/file"])
        setup_result = execute.Output(returncode=1)
        executor = utils.mock_executor(setup_result)

        self.assertFalse(main.run_tests([t], executor, observer))

    def test_erroring_test(self):
        observer = utils.NullObserver()
        t = test.Test(
            description="Running out of lines!",
            command="echo '123abc'\n",
            output_specs=["2: 123abc\n"],
        )
        tests = [t]
        result = execute.Output(returncode=0, stdout="123abc\n")
        error = execute.OutOfLinesError(result, "stderr")
        executor = utils.mock_executor(result)

        with mock.patch("scruf.runner.compare_result", side_effect=error):
            success = main.run_tests(tests, executor, observer)
        self.assertFalse(success)


class TestRunTestsObserverNotifications(unittest.TestCase):
    def test_observations_with_successful_test(self):
        observer = mock.Mock()
        t = test.Test(
            description="printf prints to stdout\n",
            command="printf 'foo\n1bar\n'\n",
            output_specs=[
                parsers.OutputParser.parse(line)
                for line in ["foo\n", "[RE] [0-9]bar\n", "[0]\n"]
            ],
        )
        result = execute.Output(returncode=0, stdout="foo\n1bar\n")
        executor = utils.mock_executor(result)
        tests = [t]

        # capture to avoid polluting test output
        with utils.Capturer():
            main.run_tests(tests, executor, observer)

        # Check we called expected methods
        observer.notify_before_tests_run.assert_called_once_with(tests)
        observer.notify_before_test_run.assert_called_once_with(t, 1)
        observer.notify_test_success.assert_called_once_with(t, 1)

        # and didn't call anything else
        observer.notify_test_error.assert_not_called()
        observer.notify_test_comparison_failure.assert_not_called()

    def test_observations_with_failed_test_comparisons_and_successful_strict(self):
        observer = mock.Mock()
        t = test.Test(
            description="exit doesn't set returncode\n",
            command="exit 0\n",
            output_specs=["[1]"],
        )
        result = execute.Output(returncode=0)
        executor = utils.mock_executor(result)
        tests = [t]
        comparison_results = [factories.runner_result_factory(success=False)]

        with utils.Capturer():
            with mock.patch(
                "scruf.runner.compare_result", return_value=comparison_results
            ):
                main.run_tests(tests, executor, observer, strict=True)

        observer.notify_before_tests_run.assert_called_once_with(tests)
        observer.notify_before_test_run.assert_called_once_with(t, 1)
        observer.notify_test_comparison_failure.assert_called_once_with(
            t, 1, comparison_results
        )

        observer.notify_test_error.assert_not_called()
        observer.notify_test_success.assert_not_called()

    def test_observations_with_strict_test_failure_and_successful_comparisons(self):
        observer = mock.Mock()
        t = test.Test(
            description="I only need to check one line\n",
            command="printf 'foo\nbar\n'\n",
            output_specs=["foo\n"],
        )
        result = execute.Output(returncode=0, stdout="foo\nbar\n")
        executor = utils.mock_executor(result)
        tests = [t]
        comparison_results = [{"success": True}]

        with utils.Capturer():
            with mock.patch(
                "scruf.runner.compare_result", return_value=comparison_results
            ):
                main.run_tests(tests, executor, observer, strict=True)

        observer.notify_before_tests_run.assert_called_once_with(tests)
        observer.notify_before_test_run.assert_called_once_with(t, 1)
        observer.notify_test_strict_failure.assert_called_once_with(
            t, 1, {"stdout": ["foo\n", "bar\n"]}
        )

        observer.notify_test_comparison_failure.assert_not_called()
        observer.notify_test_error.assert_not_called()
        observer.notify_test_success.assert_not_called()

    def test_observations_with_setup_failure(self):
        observer = mock.Mock()
        setup_command = "echo 'content' > file_without_write_perms"
        t = test.Test(
            setup_commands=[setup_command],
            command="cat file_without_write_perms",
        )
        setup_result = execute.Output(returncode=3)
        executor = utils.mock_executor(setup_result)
        tests = [t]

        with utils.Capturer():
            main.run_tests(tests, executor, observer)

        observer.notify_before_tests_run.assert_called_once_with(tests)
        observer.notify_before_test_run.assert_called_once_with(t, 1)
        observer.notify_setup_command_failure.assert_called_once_with(
            t, 1, setup_command, setup_result
        )

        observer.notify_test_success.assert_not_called()
        observer.notify_test_comparison_failure.assert_not_called()

    def test_observations_with_error(self):
        observer = mock.Mock()
        t = test.Test(
            description="I made a regex error",
            command="echo '123abc'\n",
            output_specs=["[RE] [0-9\n"],
        )
        error = compare.RegexError("[0-9", Exception("Error!"))
        tests = [t]
        result = execute.Output(returncode=0, stdout="123abc\n")
        executor = utils.mock_executor(result)

        with utils.Capturer():
            with mock.patch("scruf.runner.compare_result", side_effect=error):
                main.run_tests(tests, executor, observer)

        observer.notify_before_tests_run.assert_called_once_with(tests)
        observer.notify_before_test_run.assert_called_once_with(t, 1)
        observer.notify_test_error.assert_called_once_with(t, 1, error)

        observer.notify_test_comparison_failure.assert_not_called()
        observer.notify_test_success.assert_not_called()
