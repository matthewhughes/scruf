import unittest

from scruf import compare, execute, parsers, runner, test
from tests import factories


class TestCompare(unittest.TestCase):
    def test_comparison(self):
        t = test.Test(
            command="printf 'foo\nbar\n\tbuz\t\n'\n",
            output_specs=[
                parsers.OutputParser.parse(line)
                for line in ["foo\n", "[RE] [0-9]bar\n", r"[ESC] \tbuz\t" + "\n", "[0]"]
            ],
        )
        result = execute.Output(0, "foo\nbar\n\tbuz\t\n")

        expected_results = [
            runner.Result(
                success=True,
                type=compare.ComparisonTypes.BASIC,
                result_line="foo\n",
                result_source=test.OutputSpec.Sources.STDOUT,
                test_line="foo\n",
            ),
            runner.Result(
                success=False,
                type=compare.ComparisonTypes.REGEX,
                result_line="bar\n",
                result_source=test.OutputSpec.Sources.STDOUT,
                test_line="[0-9]bar",
            ),
            runner.Result(
                success=True,
                type=compare.ComparisonTypes.ESCAPE,
                result_line="\tbuz\t\n",
                result_source=test.OutputSpec.Sources.STDOUT,
                test_line="\tbuz\t\n",
            ),
            runner.Result(
                success=True,
                type=compare.ComparisonTypes.RETURNCODE,
                result_line="0",
                result_source=test.OutputSpec.Sources.RETURNCODE,
                test_line="0",
            ),
        ]

        for i, out in enumerate(runner.compare_result(t, result)):
            expected = expected_results[i]
            self.assertEqual(out.result_line, expected.result_line)
            self.assertEqual(out.result_source, expected.result_source)
            self.assertEqual(out.test_line, expected.test_line)
            self.assertEqual(out.success, expected.success)
            self.assertEqual(out.type, expected.type)


class TestFailureLines(unittest.TestCase):
    FAILURE_DATA = [
        {
            "message": "Basic comparison with line endings",
            "type": compare.ComparisonTypes.BASIC,
            "result_line": "foo bar\n",
            "test_line": "foo buz\n",
            "expected": [r"got: 'foo bar\n'", r"expected: 'foo buz\n'"],
        },
        {
            "message": "Basic comparison without result line endings",
            "type": compare.ComparisonTypes.BASIC,
            "result_line": "foo bar",
            "test_line": "foo buz\n",
            "expected": ["got: 'foo bar' (no eol)", r"expected: 'foo buz\n'"],
        },
        {
            "message": "No EOL comparison with non matching content",
            "type": compare.ComparisonTypes.NO_EOL,
            "result_line": "No EOL here",
            "test_line": "No EOL here but some more text",
            "expected": [
                "got: 'No EOL here' (no eol)",
                "expected: 'No EOL here but some more text' (no eol)",
            ],
        },
        {
            "message": "No EOL comparison with line ending in result",
            "type": compare.ComparisonTypes.BASIC,
            "result_line": "No EOL here\n",
            "test_line": "No EOL here",
            "expected": [r"got: 'No EOL here\n'", "expected: 'No EOL here' (no eol)"],
        },
        {
            "message": "Regex match failure",
            "type": compare.ComparisonTypes.REGEX,
            "result_line": "123 abc\n",
            "test_line": "^[0-9]+ [A-Z]",
            "expected": [r"got: '123 abc\n'", "Does not match: '^[0-9]+ [A-Z]'"],
        },
        {
            "message": "Returncode match failure",
            "type": compare.ComparisonTypes.RETURNCODE,
            "result_line": 0,
            "test_line": 1,
            "expected": ["got return code: 0 != 1"],
        },
        {
            "message": "Escaped content match failure",
            "type": compare.ComparisonTypes.ESCAPE,
            "result_line": "foo\t\tbar\n",
            "test_line": "foo\tbar" + "\n",
            "expected": [r"got: 'foo\t\tbar\n'", r"expected: 'foo\tbar\n'"],
        },
    ]

    def test_failures(self):
        for data in self.FAILURE_DATA:
            with self.subTest(data["message"]):
                comparison = factories.runner_result_factory(
                    type=data["type"],
                    result_line=data["result_line"],
                    test_line=data["test_line"],
                )
                self.assertEqual(comparison.get_printable_failures(), data["expected"])
