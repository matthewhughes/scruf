import contextlib
import io
import sys
from unittest import mock

from scruf import observe


def mock_executor(result):
    executor = mock.Mock()
    executor.execute = mock.Mock(return_value=result)
    return executor


class Capturer(contextlib.AbstractContextManager):
    def __enter__(self):
        self._orig_stdout = sys.stdout
        self._orig_stderr = sys.stderr

        sys.stdout = self._stdout_io = io.StringIO()
        sys.stderr = self._stderr_io = io.StringIO()
        return self

    def __exit__(self, *args):
        self.stdout = self._stdout_io.getvalue()
        self.stderr = self._stderr_io.getvalue()

        # restore streams
        sys.stdout = self._orig_stdout
        sys.stderr = self._orig_stderr
        del self._stdout_io
        del self._stderr_io


class NullObserver(observe.Observer):
    """Observer class that outputs nothing"""

    def notify_file_open_error(self, filename, exception):
        pass

    def notify_test_progression_error(self, filename, exception):
        pass

    def notify_execute_setup_error(self, filename, exception):
        pass
