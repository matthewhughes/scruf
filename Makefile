PYTHON ?= python3
PREFIX ?= /usr/local
.DEFAULT_GOAL := help

NAME := scruf
LIB_DIR := scruf
TEST_DIR := tests
PYTHON_FILE_PATHS := $(LIB_DIR) $(TEST_DIR) bin/$(NAME) setup.py

.PHONY: help build docs install dist unit_test test_coverage \
	test_end_to_end test lint format_code check_code_format verify

help:
	@echo -e "The python version to run recipes under can be specified via the \
	'PYTHON' environment variable, e.g."
	@echo -e "\tPYTHON=/usr/bin/python3.6 make unit_test"
	@echo -e ""
	@echo -e "Convenience recipes:"
	@echo -e "\ttest : run unit and end-to-end tests"
	@echo -e "\tverify : run test, linter, and verify code is correctly \
	formatted. This should pass on every commit"
	@echo -e ""
	@echo -e "Useful recipes:"
	@echo -e "\tunit_test: run unit tests"
	@echo -e "\ttest_coverage: run unit tests and report coverage"
	@echo -e "\ttset_end_to_end: build the program and run end-to-end tests on 'examples/' directory"
	@echo -e "\tlint: run linter"
	@echo -e "\tformat_code: run code formatter"
	@echo -e "\tsort_imports: sort imports in Python code"

build:
	$(PYTHON) setup.py build --build-scripts build/scripts

docs:
	sphinx-build -d docs/build/doctrees -W docs/source docs/build/html

install: build
	$(PYTHON) setup.py install --prefix="$(PREFIX)" --force

dist:
	$(PYTHON) setup.py sdist bdist_wheel

unit_test:
	pytest $(TEST_DIR)

test_coverage:
	coverage run --branch --source $(TEST_DIR),$(LIB_DIR) -m pytest
	@# All test code should be run
	coverage report --fail-under 100 --show-missing --skip-covered --include \
		"./$(TEST_DIR)/*"
	coverage report --fail-under 90 --show-missing --skip-covered --include \
		"./$(LIB_DIR)/*"

test_end_to_end: build
	./dev-bin/run_examples.sh $(shell pwd)/build/lib build/scripts

test: test_coverage test_end_to_end

lint:
	flake8 $(PYTHON_FILE_PATHS)
	isort --check $(PYTHON_FILE_PATHS)
	mypy --strict $(LIB_DIR)

format_code:
	black $(PYTHON_FILE_PATHS)

check_code_format:
	black --check $(PYTHON_FILE_PATHS)

sort_imports:
	isort $(PYTHON_FILE_PATHS)

verify: test lint check_code_format
