Test Results
============

A test is considered to have failed if any one of its comparisons fail. If you
would like the additional constraint of ensuring a comparison was made for each
line of output you can pass the ``--strict`` flag. In strict mode any test
that doesn't explicitly test each line of output is marked as a failure.

Scruf Output
------------

By default ``scruf`` will output results following TAP_ (Test Anything
Protocol). Other formats, e.g. JUnit should be coming in the future.

.. _TAP: http://testanything.org

