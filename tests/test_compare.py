import unittest

from scruf import compare


class TestComprerFactory(unittest.TestCase):
    SUCCESSFUL_COMPARISONS = [
        {
            "type": compare.ComparisonTypes.BASIC,
            "expected": "foo bar\n",
            "content": "foo bar\n",
        },
        {
            "type": compare.ComparisonTypes.REGEX,
            "expected": "^[a-z]+$",
            "content": "abcdef",
        },
        {
            "type": compare.ComparisonTypes.NO_EOL,
            "expected": "No newline here!",
            "content": "No newline here!",
        },
        {"type": compare.ComparisonTypes.RETURNCODE, "expected": 1, "content": 1},
        {
            "type": compare.ComparisonTypes.ESCAPE,
            "expected": "Already processed:\tNo special processing",
            "content": "Already processed:\tNo special processing",
        },
    ]

    def test_successful_comparisons(self):
        for test in self.SUCCESSFUL_COMPARISONS:
            with self.subTest(test["type"]):
                comparer = compare.get_comparer(test["type"])
                self.assertTrue(comparer(test["expected"], test["content"]))

    FAILING_COMPARISONS = [
        {
            "message": "Non-matching basic comparison",
            "type": compare.ComparisonTypes.BASIC,
            "expected": "foo bar\n",
            "content": "buz bux\n",
        },
        {
            "message": "No end of line comparison with end of line",
            "type": compare.ComparisonTypes.NO_EOL,
            "expected": "No newline here!",
            "content": "No newline here!\n",
        },
    ]

    def test_failing_comparisons(self):
        for test in self.FAILING_COMPARISONS:
            with self.subTest(test["message"]):
                comparer = compare.get_comparer(test["type"])
                self.assertFalse(comparer(test["expected"], test["content"]))

    def test_build_with_invalid_regex(self):
        invalid_regex = "^[ a-z"
        comparer = compare.get_comparer(compare.ComparisonTypes.REGEX)
        with self.assertRaises(compare.RegexError) as cm:
            comparer(invalid_regex, "abc def")

        self.assertEqual(cm.exception.regex, invalid_regex)
