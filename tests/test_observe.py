import unittest

from scruf import observe
from tests import utils


class TestObserver(unittest.TestCase):
    """Test expected methods exist, take the expected number of parameters, and noop"""

    def setUp(self):
        self.observer = observe.Observer()

    def test_notify_file_open_error(self):
        filename = "unopenable"
        exception = PermissionError("No permissions")
        with utils.Capturer() as captured:
            self.observer.notify_file_open_error(filename, exception)
        self.assertEqual(
            captured.stderr,
            "Failed to open file " + filename + ": " + str(exception) + "\n",
        )

    def test_notify_test_progression_error(self):
        filename = "bad_test_data"
        exception = Exception("Invalid test")

        with utils.Capturer() as captured:
            self.observer.notify_test_progression_error(filename, exception)

        self.assertEqual(
            captured.stderr,
            "Error when parsing " + filename + ": " + str(exception) + "\n",
        )

    def test_notify_execute_setup_error(self):
        filename = "normal_test"
        exception = Exception("Building executor failed")

        with utils.Capturer() as captured:
            self.observer.notify_execute_setup_error(filename, exception)
        self.assertEqual(
            captured.stderr,
            "Failed to setup test environment for "
            + filename
            + ": "
            + str(exception)
            + "\n",
        )

    def test_notify_before_testing_file(self):
        filename = "test_file.scf"
        self.assertIsNone(self.observer.notify_before_testing_file(filename))

    def test_notify_before_tests_run(self):
        tests = []
        self.assertIsNone(self.observer.notify_before_tests_run(tests))

    def test_notify_before_test_run(self):
        test = object()
        test_number = 1
        self.assertIsNone(self.observer.notify_before_test_run(test, test_number))

    def test_notify_setup_command_failure(self):
        test = object()
        test_number = 1
        setup_command = "echo 'foo' > test.txt"
        result = object()
        self.assertIsNone(
            self.observer.notify_setup_command_failure(
                test, test_number, setup_command, result
            )
        )

    def test_notify_test_error(self):
        error = Exception()
        test = object()
        test_number = 1
        self.assertIsNone(self.observer.notify_test_error(test, test_number, error))

    def test_notify_test_success(self):
        test = object()
        test_number = 1
        self.assertIsNone(self.observer.notify_test_success(test, test_number))

    def test_notify_test_comparison_failure(self):
        test = object()
        test_number = 1
        failed_comparisons = []
        self.assertIsNone(
            self.observer.notify_test_comparison_failure(
                test, test_number, failed_comparisons
            )
        )

    def test_notify_test_strict_failure(self):
        test = object()
        test_number = 1
        source = "stdout"
        remaining_lines = ["some content\n"]
        self.assertIsNone(
            self.observer.notify_test_strict_failure(
                test, test_number, {source: remaining_lines}
            )
        )
