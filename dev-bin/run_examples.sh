#!/bin/sh

# Usage: ./run_examples.sh [ libray_path bin_path ]

set -eu

LIB_ROOT="${1:-$PWD}"
BIN_PATH="${2:-bin}"

alias scruf="$BIN_PATH/scruf"
export PYTHONPATH="$LIB_ROOT"

BOLD_GREEN="\\033[32;1m"
COLOR_RESET="\\033[m"

test_failure () {
    set +e
    scruf "$@"
    retval=$?
    set -e
    [ $retval -ne 0 ]
}

scruf examples/*.scf
scruf examples/explicit_testdir/with_testdir.scf
scruf --indent '\t' examples/different_indentation/with_tabs.scf
scruf --env-file examples/injecting_env/scruf.cfg \
    examples/injecting_env/test_environment_injection.scf
scruf --strict examples/strict_testing/basic_output_with_strict.scf

echo -e "#####${BOLD_GREEN}TESTING EXPECTED FAILURES${COLOR_RESET}#####"
for f in examples/failures/*.scf
do
    test_failure "$f"
done
test_failure --strict examples/strict_testing/failures.scf
echo -e "#####${BOLD_GREEN}END FAILURE TESTING${COLOR_RESET}#####"
