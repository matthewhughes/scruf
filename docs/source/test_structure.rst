Test Structure
==============

This is a summary of the structure of tests expected by ``scruf``, these tests
contain:

* A optional description_ lines, followed by
* Optional `setup command`_ to run, followed by
* A test `test command`_ to run, followed by
* Optional `output specification`_ to compare against the result of running
  the command.

There are examples, run as part of the test suite, present under ``examples/``

Comments
--------

Any line beginning with a ``#`` is treated as a comment, and is ignored.

.. _description:

Test Descriptions
-----------------

Non-comment lines beginning with anything other than a space of ``$`` are
interpreted as descriptions. These are used as descriptions in the test output

For example::

   # This is a comment
   This would be be a test description

.. _setup command:

Setup Command
-------------

Lines beginning with a ``[SETUP]$`` are interpreted as setup commands, these
are commands to be run before the actual test and whose output is not used as
part of a test. If one of these commands fails (i.e. returns a non-zero value)
the test is marked as failed. e.g.::

   # This is a comment
   This would be a test description
   # Add some content to 'my_file'
   [SETUP]$ echo 'some content' > my_file

.. _test command:

Test Commands
-------------

Commands are specified by a line indented (by default with 4 spaces, but this
can be configured with the ``--indent`` option) followed by ``$``. Building on
our example::

   # This is a comment
   This would be a test description
   # Add some content to 'my_file'
   [SETUP]$ echo 'some content' > my_file
        $ cat my_file

Commands can continued over more than one lines, such continuations are marked
by an indented line beginning with ``>``, e.g.::

   Concatentate two files
       $ cat first_file
       > second_file

This would be equivalent to::

   Concatenate two files
       $ cat first_file second_file

.. _output specification:

Output Specification
--------------------

Output is defined by a indented line beginning with anything other than ``$``
or ``>``. The simplest comparison that can be made on output is a direct
comparison on contents, our complete test is then::

   # This is a comment
   This would be a test description
   # Add some content to 'my_file'
   [SETUP]$ echo 'some content' > my_file
        $ cat my_file
        some content

Stream Specification
~~~~~~~~~~~~~~~~~~~~

The output stream, standard out or standard error, can be specified by
prefixing an output line with ``1:`` or ``2:`` respectively, for example::

   'printf' prints to stdout
       $ printf "Off I go\n"
       1: Off I go

   Output can be redirected
       $ printf "Error!\n" >&2
       2: Error!
       
Regex Comparisons
~~~~~~~~~~~~~~~~~

Output can be compared against a provided regex using the flag: ``[RE]``, for
example::

   Printing numbers
       $ echo '1234'
       [RE] ^\d+$

To combine this with stream specification simply specify the stream before the
regex flag::

   Numbers to stderr
       $ echo '1234' >&2
       2:[RE] ^\d+$


Comparisons Without End-Of-Line Characters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An output line without a line ending can be tested using the No End of Line
flag: ``[NEoL]``, for example::

   'printf' doesn't add newlines
       $ printf "Hello, world"
       [NEoL] Hello, world


Exit Code Comparisons
~~~~~~~~~~~~~~~~~~~~~

Line's which contain only an integer contained in ``[]`` are used to test the
exit code of a command, for example::

   # Intentionally disregard output
   'echo' exits with 0 on success
       $ echo "Everything is ok"
       [0]

   'exit' sets the exit code
       $ exit 1
       [1]

Combining Comparisons
~~~~~~~~~~~~~~~~~~~~~

Any combination of comparisons can be used within a single test::

   Printf with varied output   
       $ printf "Lots of interesting output\n12345\nIn this test"
       1: Lots of interesting output
       [RE] ^[0-9]+$
       [NEoL] In this test
       [0]
