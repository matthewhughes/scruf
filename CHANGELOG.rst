=========
CHANGELOG
=========

Version 0.4.1 2020-10-08
========================

* Change setup command specification from ``$`` to ``[SETUP]$``
* Extract documentation from README

Version 0.4 2020-07-11
======================

* Add ``--strict`` flag to run test in strict mode to mark test as failed if
  each line of output is not tested
* Add ``-e/--env-file`` to simplify injecting environment variables for testing
* Add ``--version`` flag
* Add end-to-end testing for expected failures

Version 0.3 2020-06-15
======================

* Add setup commands which can be run to establish the environment for a test.
* Add printing of filename to test output
* Refactor runner to use an observer pattern to make it easier to implement
  different output formats.
* Add more feedback when more lines are requested in a test output than the
  command gave.

Version 0.2 2020-06-09
======================

* Add ``-s/--shell`` to allow specifying the shell to be used for tests.
* Add ``--no-cleanup`` option to avoid deleting test directories at the end of
  a run.
* Add ``-i/--indent`` option to allow specifying the indentation used in test
  files.
* Add more complete documentation to README.
* Add more examples under ``examples/``, and a script to run these as
  end-to-end testing.
* Add ``make`` recipes: ``test_end_to_end`` for running examples, ``verify`` as
  a convenience to run all tests and linting.
* Add ``TESTDIR`` variable which is injected into the environment when running
  tests.

Version 0.1 2020-05-20
======================

* Initial release
