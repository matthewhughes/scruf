============
CONTRIBUTING
============

Feedback and pull requests are most welcome!

Getting Started
===============

Scruf should run on any Python version >= 3.6, and setup should require only
installing dependencies::
    
    $ pip3 install -r requirements_dev.txt
    $ make verify

Merge Requests
==============

Please ensure any changes you make pass the test suites and adhere to linting
and code format standards, these can all be checked by running ``make verify``

Makefile Rules
================

A list of these rules is also available via ``make help``

Testing
~~~~~~~

* ``make unit_test`` - Runs only the unit test suit
* ``make test_coverage`` - Runs the unit test suit with coverage
* ``make test_end_to_end`` - Runs the end-to-end test suit
* ``make test`` - Same as ``make test_coverge test_end_to_end``

Linting
~~~~~~~

* ``make lint`` - Lint Python code

Tidying
~~~~~~~

* ``make format_code`` - Format Python code
* ``make sort_imports`` - Sort Python imports
