from scruf import compare, runner


def runner_result_factory(
    result_line="",
    result_source="",
    test_line="",
    success=True,
    type=compare.ComparisonTypes.BASIC,
):
    """Build a `compare.runner.Result` with reasonable defaults for testing"""
    return runner.Result(result_line, result_source, test_line, success, type)
