import os
import re
import shutil
import unittest

from scruf import execute


class TestExecutorExecute(unittest.TestCase):
    def test_returncode_with_no_output(self):
        command = "exit 0"
        executor = execute.Executor()

        result = executor.execute(command)
        self.assertEqual(result.stdout, "")
        self.assertEqual(result.stderr, "")
        self.assertEqual(result.returncode, 0)

    def test_output(self):
        command = "echo 'foo'; echo 'bar' >&2"
        executor = execute.Executor()

        result = executor.execute(command)
        self.assertEqual(result.stdout, "foo\n")
        self.assertEqual(result.stderr, "bar\n")

    def test_command_runs_in_tmpdir(self):
        contents = "hello\n"
        filename = "test.txt"
        command = "echo -n '" + contents + "' >" + filename

        executor = execute.Executor()
        testdir = executor.testdir
        expected_path = os.path.join(testdir, filename)

        executor.execute(command)
        self.assertTrue(os.path.isdir(testdir))
        self.assertTrue(os.path.isfile(expected_path))

        with open(expected_path) as f:
            got_contents = f.read()
        self.assertEqual(got_contents, contents)

    def test_command_runs_with_env(self):
        env = os.environ
        test_env_key = "_CRAM_ENV_TEST"
        test_env_value = "123"
        env[test_env_key] = test_env_value

        command = "echo -n $_CRAM_ENV_TEST"
        executor = execute.Executor(env=env)

        result = executor.execute(command)
        self.assertEqual(result.stdout, test_env_value)


class TestExecuterCleanup(unittest.TestCase):
    def test_cleanup(self):
        for cleanup in [True, False]:
            with self.subTest("cleanup=" + str(cleanup)):
                command = "exit 0"
                executor = execute.Executor(cleanup=cleanup)
                testdir = executor.testdir

                executor.execute(command)

                self.assertTrue(os.path.isdir(testdir))
                del executor

                if cleanup:
                    self.assertFalse(os.path.isdir(testdir))
                else:
                    self.assertTrue(os.path.isdir(testdir))
                    # cleanup after ourselves
                    shutil.rmtree(testdir)


class TestTempDirCreationFailure(unittest.TestCase):
    # Potentially platform specific tests
    def test_file_not_found_condition(self):
        non_existant_dir = "/foo/bar/buz/scruf"

        with self.assertRaisesRegex(
            execute.FailedToCreateTestDirError,
            "Could not create temporary directory at "
            + re.escape(non_existant_dir + ":"),
        ) as cm:
            execute.Executor(tmpdir=non_existant_dir)

        exception = cm.exception
        self.assertEqual(exception.dir_name, non_existant_dir)


class TestOutput(unittest.TestCase):
    def test_overruning_output_specs_with_no_output(self):
        result = execute.Output(0)

        for stream in execute.Output.Streams:
            with self.subTest("When calling 'next_line' on {}".format(stream)):
                expected_error = (
                    "No more lines available for "
                    + "{}".format(stream)
                    + ":\n"
                    + "\tstdout was: (no content)\n"
                    + "\tstderr was: (no content)\n"
                )

                with self.assertRaises(execute.OutOfLinesError) as cm:
                    result.next_line(stream)

                exception = cm.exception
                self.assertEqual(exception.stream, stream)
                self.assertEqual(exception.result, result)
                self.assertEqual(str(exception), expected_error)

    def test_overruning_output_specs_with_output(self):
        output = "Just some text\n"
        result = execute.Output(0, stdout=output)

        expected_error = (
            "No more lines available for stderr:\n"
            + "\tstdout was: "
            + output
            + "\tstderr was: (no content)\n"
        )

        with self.assertRaises(execute.OutOfLinesError) as cm:
            result.next_line(result.Streams.STDERR)

        exception = cm.exception
        self.assertEqual(str(exception), expected_error)

    def test_has_remaining_lines(self):
        output = "Two line\nof output\n"
        result = execute.Output(0, stdout=output)

        self.assertTrue(result.has_remaining_lines(result.Streams.STDOUT))
        self.assertFalse(result.has_remaining_lines(result.Streams.STDERR))

        result.next_line(result.Streams.STDOUT)
        self.assertTrue(result.has_remaining_lines(result.Streams.STDOUT))
        result.next_line(result.Streams.STDOUT)
        self.assertFalse(result.has_remaining_lines(result.Streams.STDOUT))

    def test_get_reminaing_lines(self):
        output_lines = ["some output\n", "lines\n", "go here\n"]
        result = execute.Output(0, stdout="".join(output_lines))

        self.assertEqual(
            result.get_remaining_lines(result.Streams.STDOUT), output_lines
        )

        result.next_line(result.Streams.STDOUT)
        self.assertEqual(
            result.get_remaining_lines(result.Streams.STDOUT), output_lines[1:]
        )


class TestEnvSetup(unittest.TestCase):
    def test_env_defaulting(self):
        dirname = os.getcwd()
        executor = execute.Executor()
        self.assertEqual(executor.env["TESTDIR"], dirname)

    def test_env_vars_are_not_stomped(self):
        testdir = "testdir"
        env = {"TESTDIR": testdir}
        executor = execute.Executor(env=env)

        self.assertEqual(executor.env["TESTDIR"], testdir)
