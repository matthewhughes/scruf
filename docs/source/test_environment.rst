Test Environment
================

Each test is run in a separate directory under ``/tmp``, each directory's name
will end in ``scruf`` . By default these directories will be removed after a
test run, though this can be prevented by passing the ``--no-cleanup`` flag.

Environment Variables
---------------------

Environment variables can be passed when testing either by passing them down:

::

   $ FOO=12 BAR=13 scruf some_test.scf

Or by storing them in a config file under they key: ``[Scruf Env]``

::

   # in test.cfg
   [Scruf Env]
   FOO=12
   BAR=13

Then calling ``scruf`` with ``-e/--env-file``:

::

   $ scruf --env-file my.cfg some_test.scf
