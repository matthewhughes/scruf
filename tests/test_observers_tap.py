import unittest
from unittest import mock

from scruf import test
from scruf.observers import tap
from tests import factories, utils


class TestObserverOutput(unittest.TestCase):
    _test_description = "Test description"
    _default_test = test.Test("echo 'placeholder'", description=_test_description)

    def setUp(self):
        self.observer = tap.TapObserver()

    def test_notify_before_testing_file(self):
        filename = "test_file.scf"
        with utils.Capturer() as captured:
            self.observer.notify_before_testing_file(filename)
        self.assertEqual(captured.stdout, "# Testing: " + filename + "\n")

    def test_notify_before_test_run(self):
        tests = [1, 2, 3]
        with utils.Capturer() as captured:
            self.observer.notify_before_tests_run(tests)
        self.assertEqual(captured.stdout, "1..3\n")

    def test_notify_test_error_with_test_description(self):
        error_str = "Error!"
        description = "Test description"
        error = Exception(error_str)
        t = test.Test("echo 'ok'", description=description)
        test_number = 1

        with utils.Capturer() as captured:
            self.observer.notify_test_error(t, test_number, error)
        self.assertEqual(captured.stdout, "not ok 1 - " + description + "\n")
        self.assertEqual(captured.stderr, "# \tError!\n")

    def test_notify_test_success_with_description(self):
        with utils.Capturer() as captured:
            self.observer.notify_test_success(self._default_test, 1)
        self.assertEqual(captured.stdout, "ok 1 - " + self._test_description + "\n")

    def test_notify_test_success_without_description(self):
        t = test.Test("exit 0")
        with utils.Capturer() as captured:
            self.observer.notify_test_success(t, 1)
        self.assertEqual(captured.stdout, "ok 1\n")

    def test_notify_test_comparison_failure_with_description(self):
        failure_str = "Failed because it didn't succeed"
        comparisons = [failure_str]

        with mock.patch(
            "scruf.runner.Result.get_printable_failures", return_value=comparisons
        ):
            with utils.Capturer() as captured:
                self.observer.notify_test_comparison_failure(
                    self._default_test, 1, [factories.runner_result_factory()]
                )
        self.assertEqual(captured.stdout, "not ok 1 - " + self._test_description + "\n")
        self.assertEqual(captured.stderr, "# \t{}\n".format(failure_str))

    def test_notify_test_strict_failure(self):
        remaining_lines = ["some content\n", "goes here\n"]
        source = "stdout"
        expected_message_pargs = ["# Content still remaining for " + source + ":\n"] + [
            "# \t" + line for line in remaining_lines
        ]

        with utils.Capturer() as captured:
            self.observer.notify_test_strict_failure(
                self._default_test, 1, {"stdout": remaining_lines}
            )
        self.assertEqual(captured.stdout, "not ok 1 - " + self._test_description + "\n")
        self.assertEqual(captured.stderr, "".join(expected_message_pargs))
