Welcome to Scruf's documentation!
=================================

Scruf is a functional testing framework for command line applications.

.. toctree::
   :maxdepth: 3
   :caption: Writing Tests:

   test_structure
   test_environment
   test_results


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
