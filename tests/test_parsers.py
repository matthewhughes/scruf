import unittest

from scruf import compare, parsers, test


class TestTestParser(unittest.TestCase):
    TEST_DATA = [
        {
            "message": "Single command",
            "input": ["    $ cat my_file >/dev/null"],
            "output": test.Test("cat my_file >/dev/null"),
        },
        {
            "message": "Command with description",
            "input": ["'cat' concatenates files\n", "    $ cat my_file\n"],
            "output": test.Test("cat my_file", description="'cat' concatenates files"),
        },
        {
            "message": "Command with setup commands",
            "input": [
                "[SETUP]$ echo 'some content' > test.txt\n",
                "    $ cat test.txt\n",
            ],
            "output": test.Test("cat test.txt", setup_commands=["echo 'some content'"]),
        },
        {
            "message": "Command with continue",
            "input": ["    $ cat my_file\n", "    > another_file"],
            "output": test.Test("cat my_file another_file"),
        },
        {
            "message": "Command with result",
            "input": ["    $ cat my_file\n", "    foo bar"],
            "output": test.Test(
                "cat my_file", output_specs=[parsers.OutputParser.parse("foo bar")]
            ),
        },
        {
            "message": "Command with result with newline",
            "input": ["    $ cat my_file\n", "    foo bar\n"],
            "output": test.Test(
                "cat my_file", output_specs=[parsers.OutputParser.parse("foo bar\n")]
            ),
        },
        {
            "message": "Command with description and continue",
            "input": [
                "'cat' concatenates files\n",
                "    $ cat my_file\n",
                "    > another_file",
            ],
            "output": test.Test(
                "cat my_file another_file", description="'cat' concatenates files"
            ),
        },
        {
            "message": "Command with description and setup commands",
            "input": [
                "Write to and read from a file\n",
                "[SETUP]$ echo 'input content' > read_me_first.txt\n",
                "[SETUP]$ echo 'more input' > read_me_second.txt\n",
                "    $ cat read_me_first.txt read_me_second.txt\n",
            ],
            "output": test.Test(
                "cat read_me_first.txt read_me_second.txt",
                description="Write to and read from a file",
                setup_commands=[
                    "echo 'input_content' > read_me_first.txt",
                    "echo 'more input' > read_me_second.txt",
                ],
            ),
        },
        {
            "message": "Command with comments and empty lines",
            "input": [
                "# Double check 'cat' still works on empty file\n",
                "    $ cat my_file\n",
                "\n",
                "",
            ],
            "output": test.Test("cat my_file"),
        },
    ]

    def _compare_result(self, test):
        parser = parsers.TestParser()
        got = parser.parse(test["input"])

        self.assertEqual(len(got), 1)
        self._compare_tests(got[0], test["output"])

    def _compare_tests(self, got_test, expected_test):
        self.assertEqual(got_test.command, expected_test.command)
        self.assertEqual(got_test.description, expected_test.description)
        self.assertEqual(len(got_test.output_specs), len(expected_test.output_specs))

    def test_parsing_single_tests(self):
        for data in self.TEST_DATA:
            with self.subTest(data["message"]):
                self._compare_result(data)

    def test_parsing_multiple_tests(self):
        test_input = []
        expected_output = []
        for data in self.TEST_DATA:
            test_input += data["input"]
            expected_output.append(data["output"])

        parser = parsers.TestParser()
        got = parser.parse(test_input)
        self.assertEqual(len(got), len(expected_output))
        for i, out in enumerate(got):
            self._compare_tests(out, expected_output[i])

    def test_parse_with_invalid_progressions(self):
        lines = [
            "test with a missing command!\n",
            "    > another_file\n",
        ]

        parser = parsers.TestParser()
        with self.assertRaises(parsers.ProgressionError) as cm:
            parser.parse(lines)
        exception = cm.exception
        self.assertEqual(exception.line_num, 1)
        self.assertCountEqual(
            exception.expected_states, ["test_command", "description", "setup_command"]
        )

    def test_parse_with_different_indent(self):
        for indent in [" ", "\t"]:
            lines = [
                "This is a description\n",
                indent + "$ echo 'first arg'\n",
                indent + "> 'second arg'\n",
            ]
            expected_test = test.Test(
                "echo 'first arg' 'second arg'", description="This is a description"
            )
            parser = parsers.TestParser(indent)
            self._compare_tests(parser.parse(lines)[0], expected_test)


class TestOutputParser(unittest.TestCase):
    _TEST_DATA = [
        {
            "message": "Basic comparison",
            "input": "foo bar\n",
            "type": compare.ComparisonTypes.BASIC,
        },
        {
            "message": "Basic comparison with explicit stdout source",
            "input": "1: foo bar\n",
            "content": "foo bar\n",
            "source": test.OutputSpec.Sources.STDOUT,
            "type": compare.ComparisonTypes.BASIC,
        },
        {
            "message": "Basic comparison with explicit stderr source",
            "input": "2: Error!\n",
            "content": "Error!\n",
            "source": test.OutputSpec.Sources.STDERR,
            "type": compare.ComparisonTypes.BASIC,
        },
        {
            "message": "Basic comparison with leading space",
            "input": "1: \tfoo bar\n",
            "content": "\tfoo bar\n",
            "type": compare.ComparisonTypes.BASIC,
        },
        {
            "message": "Regex comparison with explicit source",
            "input": "1:[RE] \\d+$\n",
            "content": "\\d+$",
            "type": compare.ComparisonTypes.REGEX,
            "source": test.OutputSpec.Sources.STDOUT,
        },
        {
            "message": "No end of line comparison",
            "input": "[NEoL] foo bar\n",
            "content": "foo bar",
            "type": compare.ComparisonTypes.NO_EOL,
        },
        {
            "message": "Return code comparison",
            "input": "[1]\n",
            "content": 1,
            "type": compare.ComparisonTypes.RETURNCODE,
            "source": test.OutputSpec.Sources.RETURNCODE,
        },
        {
            "message": "Escaped content comparison",
            "input": r"[ESC] foo\n\tbar\t",
            "content": "foo\n\tbar\t",
            "type": compare.ComparisonTypes.ESCAPE,
        },
    ]

    def test_parsing(self):
        for test_data in self._TEST_DATA:
            with self.subTest(test_data["message"]):
                parse_result = parsers.OutputParser.parse(test_data["input"])

                expected_content = (
                    test_data["content"]
                    if "content" in test_data
                    else test_data["input"]
                )
                expected_source = (
                    test_data["source"]
                    if "source" in test_data
                    else test.OutputSpec.Sources.STDOUT
                )
                expected_spec = test.OutputSpec(
                    comp_type=test_data["type"],
                    content=expected_content,
                    source=expected_source,
                )
                self._compare_specs(parse_result, expected_spec)

    def _compare_specs(self, first, second):
        self.assertEqual(first.comp_type, second.comp_type)
        self.assertEqual(first.source, second.source)
        self.assertEqual(first.content, second.content)


class TestGetNamedProgressions(unittest.TestCase):
    def test_with_state_that_progresses_to_start(self):
        parser = parsers.TestParser()
        fsm = parser.fsm
        state = fsm.States.TEST_COMMAND

        expected_names = [
            "continue",
            "result",
            "description",
            "test_command",
            "setup_command",
        ]

        self.assertCountEqual(fsm.get_named_progressions(state), expected_names)


class TestEscapeSpace(unittest.TestCase):
    def test_with_non_escaped_string(self):
        string = "Not much of interest here"
        self.assertEqual(parsers.escape_space(string), string)

    def test_with_escaped_spaces(self):
        string = r"Spaces\sin\sthis\sstring"
        expected = "Spaces in this string"
        self.assertEqual(parsers.escape_space(string), expected)

    def test_with_escaped_tabs(self):
        string = r"Tab\tseparated\ttext"
        expected = "Tab\tseparated\ttext"
        self.assertEqual(parsers.escape_space(string), expected)
